Name:	       etmem
Version:       1.1
Release:       9
Summary:       etmem 
License:       MulanPSL-2.0
URL:           https://gitee.com/openeuler/etmem
Source0:       https://gitee.com/openeuler/etmem/repository/archive/%{version}.tar.gz

Patch0000:     0001-etmem-1.1-add-loongarch64-support.patch
Patch0001:     0002-etmem-remove-unnecessary-log-code.patch
Patch0002:     0003-etmem-fix-memory-leak-and-fd-leak.patch
Patch0003:     0004-etmem-fix-multiple-etmemd-and-too-many-err-log-probl.patch
Patch0004:     0005-etmem-1.1-add-riscv64-support.patch
Patch0005:     0006-etmem-fix-project-stop-cmd-timeout-problem.patch
Patch0006:     0007-etmem-construct-etmem-debug-info-package.patch
Patch0007:     0008-etmem-set-scan_type-optional-and-fix-sequence-of-head.patch

#Dependency
BuildRequires: cmake gcc gcc-c++ glib2-devel
BuildRequires: libboundscheck numactl-devel libcap-devel json-c-devel
Requires: libboundscheck json-c libcap numactl-libs
Requires: glib2

%description
etmem module

#Build sections
%prep
%autosetup -n etmem-%{version} -p1

%build
mkdir -p build
cd build
%if "%{?toolchain}" == "clang"
CFLAGS="${CFLAGS:-%{?build_cflags}} -Wno-typedef-redefinition"; export CFLAGS;
%endif
cmake .. -DCONFIG_DEBUG=y
make

%install
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_libdir}
mkdir -p $RPM_BUILD_ROOT%{_includedir}
install -d $RPM_BUILD_ROOT%{_sysconfdir}/etmem/

install -m 0700 etmem/build/bin/etmem $RPM_BUILD_ROOT%{_bindir}
install -m 0700 etmem/build/bin/etmemd $RPM_BUILD_ROOT%{_bindir}
install -m 0600 etmem/conf/damon_conf.yaml $RPM_BUILD_ROOT%{_sysconfdir}/etmem/
install -m 0600 etmem/conf/cslide_conf.yaml $RPM_BUILD_ROOT%{_sysconfdir}/etmem/
install -m 0600 etmem/conf/slide_conf.yaml $RPM_BUILD_ROOT%{_sysconfdir}/etmem/
install -m 0600 etmem/conf/thirdparty_conf.yaml $RPM_BUILD_ROOT%{_sysconfdir}/etmem/

install -m 0750 build/memRouter/memdcd $RPM_BUILD_ROOT%{_bindir}
install -m 0750 build/userswap/libuswap.a $RPM_BUILD_ROOT%{_libdir}
install -m 0644 userswap/include/uswap_api.h $RPM_BUILD_ROOT%{_includedir}
%files
%defattr(-,root,root,0750)
%attr(0500, -, -) %{_bindir}/etmem
%attr(0500, -, -) %{_bindir}/etmemd
%dir %{_sysconfdir}/etmem
%{_sysconfdir}/etmem/damon_conf.yaml
%{_sysconfdir}/etmem/cslide_conf.yaml
%{_sysconfdir}/etmem/slide_conf.yaml
%{_sysconfdir}/etmem/thirdparty_conf.yaml
%attr(0550, -, -) %{_bindir}/memdcd
%attr(0550, -, -) %{_libdir}/libuswap.a
%{_includedir}/uswap_api.h

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%changelog
* Mon Nov 4 2024 chenrenhui <chenrenhui1@huawei.com> - 1.1-9
- Type: bugfix
- CVE:NA
- SUG:NA
- DESC:set scan_type optional, and fix sequence of header file

* Tue Apr 2 2024 tangyuchen <tangyuchen5@huawei.com> - 1.1-8
- Type: bugfix
- CVE:NA
- SUG:NA
- DESC:add debug info support for etmem

* Thu Feb 22 2024 luofng <luofeng13@huawei.com> - 1.1-7
- Type: enhencement
- CVE:NA
- SUG:NA
- DESC:support for building with clang

* Thu Aug 24 2023 volcanodragon <linfeilong@huawei.com> - 1.1-6
- fix project stop cmd timeout problem

* Thu Jul 20 2023 zhangxiang <zhangxiang@iscas.ac.cn> - 1.1-5
- Add riscv64 support

* Thu Jun 8 2023 liubo <liubo254@huawei.com> 1.1-4
- backport bugfix patch from upstream

* Thu Mar 16 2023 liubo <liubo254@huawei.com> 1.1-3
- update the README file.

* Tue Mar 7  2023 Huang Yang <huangyang@loongson.cn> 1.1-2
- add loongarch64 support

* Sun Jan 29 2023 liubo <liubo254@huawei.com> 1.1-1
- upgrade etmem version to 1.1

* Thu Dec 1 2022 liubo <liubo254@huawei.com> 1.0-12
- Modify License to MulanPSL-2.0 in the spec

* Mon Aug 1 2022 liubo <liubo254@huawei.com> 1.0-11
- Sync the features and bug fixes in the etmem source repo. 

* Thu Dec 16 2021 YangXin <245051644@qq.com> 1.0-10
- Update memdcd engine for userswap page filter.

* Fri Oct 29 2021 liubo <liubo254@huawei.com> 1.0-9
- Add missing URL and source to etmem.spec

* Tue Oct 19 2021 shikemeng <shikemeng@huawei.com> 1.0-8
- Add missing Requires
- Remove write permssion in %file after strip
- Change Requires numactl to numactl-libs

* Thu Sep 30 2021 yangxin <245051644@qq.com> 1.0-7
- Update etmem and add new features memRouter and userswap.=

* Mon Aug 2 2021 louhongxiang <louhongxiang@huawei.com> 1.0-6
- cancel write permission of root.

* Mon May 24 2021 liubo <liubo254@huawei.com> 1.0-5
- add missing BuildRequires in etmem spec

* Fri Apr 2 2021 louhongxiang <louhongxiang@huawei.com> 1.0-4
- modify README correctly

* Sat Mar 20 2021 liubo <liubo254@huawei.com> 1.0-3
- Change aarch64 march to armv8-a

* Thu Mar 18 2021 liubo <liubo254@huawei.com> 1.0-2
- Fix 64K pagesize scan problem

* Thu Mar 18 2021 louhongxiang <louhongxiang@huawei.com>
- Package init
